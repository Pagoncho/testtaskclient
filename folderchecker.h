#ifndef FOLDERCHECKER_H
#define	FOLDERCHECKER_H

#include <string>
#include <vector>
#include <map>

using namespace std;

struct fileAttr {
  string file;  
  string mods;
  string owner;
  string date;
  bool isMatched;
} ;

class FolderChecker
{
    string pathToFolder;
    std::map<string,fileAttr > currentState;
    /*mods date action owner*/
    bool canCheck; 
    
public:
    std::vector<string> getChanges();
    FolderChecker(string pathToFolder);
    bool isOk(){ return canCheck; }
    
    enum {
        fileAdded = 0,
        fileChanged = 1,
        fileDeleted = 2,
        modChanged = 3,
        ownerChanged = 4
    };
    
private:
    bool isFolderExist(string path);
    fileAttr splitStringWithDelimiter(string s, string delim);
    string getEventString(int event);
    string combineStr(string file, string updtFields, string events);
    
};


#endif	/* FOLDERCHECKER_H */

