/* 
 * File:   main.cpp
 * Author: pablito
 *
 * Created on April 12, 2015, 5:14 PM
 */

#include <cstdlib>
#include "connector.h"
#include "tcpstream.h"
#include "utils.h"
#include "folderchecker.h"
#include <iostream>
#include <string.h>
#include <unistd.h>

using namespace std;

string checkCmd(int argc, char** argv, string cmd, string defaultVal, string description){
    bool isFound = false;
    string val = defaultVal;
    for(int i=0; i<argc-1; i++)
    {   if(string(argv[i]).compare(cmd) == 0)
        {
            if(i+1 >= argc)
                cout << "There is no argument for " + cmd.substr(1) + "( " + description + " )"  << endl;
            else
            {
                val.assign(string(argv[i+1]));
                isFound = true;
            } 
        }
    }
    if (!isFound) cout << cmd + " is set to " + defaultVal << endl;
    
    return val;
}

/*
 * 
 */
int main(int argc, char** argv) {

    int len;
    char line[256];
    
    string ip      = "127.0.0.1";
    int    port    = 8888;
    string folder  = "/home/pablito/test";
    int    reqTime = 5;
    
    ip = checkCmd(argc, argv, "-ip", "127.0.0.1", "ip address of the server");
    port = atoi(checkCmd(argc, argv, "-p", "8888", "port of the server application").c_str());
    folder = checkCmd(argc, argv, "-f", "/home/", "folder to check").c_str();
    reqTime = atoi(checkCmd(argc, argv, "-rt", "5", "period of requesting in sec").c_str());
    
    
    FolderChecker* fc = new FolderChecker(folder);
    if(!fc->isOk())
    {
        cout << "ERROR: Folder is not exist" << endl;
        exit(1);
    }
    
    Connector* connector = new Connector();
    TCPStream* tcpStream = connector->peerConnect(ip.c_str(), port);
    if (tcpStream) {
        string message = "start monitoring of folder: " + folder;
        cout << printMsg("INFO","TX: " + message) << endl;
        tcpStream->send(message.c_str(), message.size());
        len = tcpStream->receive(line, sizeof(line));
        line[len] = (char)NULL;
        cout << printMsg("INFO","RX: " + string(line)) << endl;
        
        while(true){
            std::vector<string> vs= fc->getChanges();
            for(int i=0; i<vs.size(); i++)
            {
                cout << printMsg("INFO","TX: " + vs.at(i)) << endl;
                tcpStream->send(vs.at(i).c_str(), vs.at(i).size());
                len = tcpStream->receive(line, sizeof(line));
                line[len] = (char)NULL;
                cout << printMsg("INFO","RX: " + string(line)) << endl;
            }
            sleep(reqTime);
        }
        
        delete tcpStream;
    }
    exit(0);
    return 0;
}

