/* 
 * File:   connector.h
 * Author: pablito
 *
 * Created on April 12, 2015, 5:14 PM
 */

#ifndef CONNECTOR_H
#define	CONNECTOR_H

#include <netinet/in.h>
#include "tcpstream.h"

class Connector
{
  public:
    TCPStream* peerConnect(const char* server, int port);
    
  private:
    int resolveHostName(const char* host, struct in_addr* addr);
};


#endif	/* CONNECTOR_H */

