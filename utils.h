/* 
 * File:   utils.h
 * Author: pablito
 *
 * Created on April 12, 2015, 5:23 PM
 */

#ifndef UTILS_H
#define	UTILS_H

#include <sys/time.h>
#include <string>
#include <stdio.h>

string printTimeNow()
{
    timeval tp;
    gettimeofday(&tp, 0);
    time_t curtime = tp.tv_sec;
    tm *t = localtime(&curtime);

    char buffer[12]; //12 - length of the string exmp: 00:22:33:123 
    sprintf(buffer,"%02d:%02d:%02d:%03d", t->tm_hour, t->tm_min, t->tm_sec, (int)(tp.tv_usec/1000) );

    return string(buffer);
}

string printMsg(string msgType, string msgComment){
    return "[ " + printTimeNow() + " ]    " + msgType +
                  "    " + msgComment; 
}

string errMsg(string comment){
    return printMsg("ERROR", comment);
} 

#endif	/* UTILS_H */

