#include "folderchecker.h"
#include <sys/stat.h>
#include <stdio.h>
#include <string.h>

FolderChecker::FolderChecker(string pathToFolder) 
{
    if(!isFolderExist(pathToFolder))
    {
        canCheck = false;
    }
    else
    {
        canCheck = true;
        this->pathToFolder = pathToFolder;
    }
}

bool FolderChecker::isFolderExist(string path)
{
    struct stat statbuf;
    
    if (stat(path.c_str(), &statbuf) != -1) 
    {
       if (S_ISDIR(statbuf.st_mode)) 
       {
           return true;
       }
    }
    return false;
}

std::vector<string> FolderChecker::getChanges()
{
    
    std::vector<string> result;
    if(!isOk())
        return result;
    
    FILE *in;
    char buff[256];

    string cmd = "ls -lrth " + pathToFolder; 
    if(!(in = popen(cmd.c_str(), "r")))
    {
        canCheck = false;
        return result;
    }

    while(fgets(buff, sizeof(buff), in)!=NULL)
    {
        string record = string(buff);
        fileAttr fa = splitStringWithDelimiter(record, "  //");
        
        if(fa.file.size() == 0)
            continue;
        
        std::map<string,fileAttr>::iterator it = currentState.find(fa.file);
        if(it == currentState.end())
        {
            string sToVec = combineStr(fa.file, fa.mods + "," + fa.owner + "," + fa.date, getEventString(fileAdded));
            result.push_back(sToVec);
            fa.isMatched = true;
            currentState[fa.file] = fa;
            continue;
        }
        
        fileAttr fb = it->second;
        string events = "";
        string updFields = "";
        if(fa.date.compare(fb.date) != 0){
            updFields = updFields + "new date: " + fa.date;
            it->second.date.assign(fa.date);
            events = events + getEventString(fileChanged);
        }
        
        if(fa.owner.compare(fb.owner) != 0){
            if(updFields.length() > 0 ) 
            {
                updFields += ","; 
                events +=  ",";
            }
            updFields = updFields + "new owner: " + fa.owner;
            it->second.owner.assign(fa.owner);
            events += getEventString(ownerChanged);
        }
        
        if(fa.mods.compare(fb.mods) != 0){
            if(updFields.length() > 0 ) 
            {
                updFields += ",";
                events +=  ",";
            }
            updFields = updFields + "new mods: " + fa.mods;
            it->second.mods.assign(fa.mods);
            events += getEventString(modChanged);
        }
        
        if(updFields.size() != 0)
        {
            string sToVec = combineStr(fa.file, updFields, events);
            result.push_back(sToVec);
        }
        it->second.isMatched = true;
    }
    
    std::map<string,fileAttr>::iterator itr=currentState.begin();
    while (itr != currentState.end()) 
    {
        if (!itr->second.isMatched) 
        {
            string sToVec = combineStr(itr->second.file, itr->second.mods + "," + itr->second.owner + "," + itr->second.date, getEventString(fileDeleted));
            result.push_back(sToVec);
            currentState.erase(itr++);
        } else 
        {
            itr->second.isMatched = false;
            ++itr;
        }
    }

    pclose(in);
}

fileAttr FolderChecker::splitStringWithDelimiter(string s, string delim){
  
    std::vector<string> result;
    char *pch;
    pch = strtok ((char*)s.c_str(),(char*)delim.c_str());
    int i = 0;
    fileAttr fa;
    fa.date = "";
    fa.isMatched = false;
    fa.file = "";
    while (pch != NULL)
    {
        string temp(pch);
        switch(i)
        {
            
            // the order of the column in ls -lrth command
            case 0:
                fa.mods.assign(temp);
                break;
           
            case 2:
                fa.owner.assign(temp);
                break;
            case 5:    
            case 6:    
            case 7:    
                 fa.date.append(temp + " ");
                 break;
            case 8:
                fa.file.assign(temp);
                fa.file.assign(fa.file.substr(0,fa.file.size() - 1));
                break;
        }
        i++;
        pch = strtok (NULL, delim.c_str());
    }
    
    return fa;
}

string FolderChecker::getEventString(int event)
{
    switch(event)
    {
        case fileAdded:
            return "added";
        case fileChanged:
            return "changed";
        case fileDeleted:
            return "deleted";
        case ownerChanged:
            return "owner changed";
        case modChanged:
            return "mods changed";
        default:
            return "unknown mod";
    }
}

string FolderChecker::combineStr(string file, string updFileds, string events){
    return "file: " + file + ", events: " + events + ",  updated fields: " + updFileds;
} 